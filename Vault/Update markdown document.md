
[- Update rich text Field – Fibery API](https://api.fibery.io/#update-rich-text-field)

## Get document id

```shell
curl -X POST https://colisweb.fibery.io/api/commands \
     -H 'Authorization: Token sometoken' \
     -H 'Content-Type: application/json' \
     -d \
      '[
         {
           "command": "fibery.entity/query",
           "args": {
             "query": {
               "q/from": "Produit/Solution",
               "q/select": [
                 "fibery/id",
                 "Produit/Name",
                 { "Produit/Description": [ "Collaboration~Documents/secret" ] }
               ],
               "q/where": ["=", ["Produit/Name"], "$name" ],
               "q/limit": 2
             },
              "params": {
              "$name": "Test End 2 End Suivi de colis"
            }
           }           
         }
      ]'
```
[Fibery API](https://api.fibery.io/#update-entity-collection-field)

## Update document
```shell
cp ~/Downloads/Export-de0bba06-5797-42ad-9285-9e88721c8035/FAQ.md /tmp/md

jq '.content =  $md' --arg md "$(< /tmp/md)"  --null-input > /tmp/json


curl -X PUT https://colisweb.fibery.io/api/documents/id?format=md \
     -H 'Authorization: Token sometoken' \
     -H 'Content-Type: application/json' \
     -d "$(</tmp/json)"
```

[How can I replace each newline (\n) with a space using sed? - Stack Overflow](https://stackoverflow.com/questions/1251999/how-can-i-replace-each-newline-n-with-a-space-using-sed)
[How to Use jq for Creating and Updating JSON Data](https://spin.atomicobject.com/2021/06/08/jq-creating-updating-json/)

**Simpler version :** [jq: pass string argument without quotes - Stack Overflow](https://stackoverflow.com/questions/52173556/jq-pass-string-argument-without-quotes)

